const CollaboratorService = {
    getCollaborators: function (token) {
        return fetch("http://localhost:3000/api/v1/collaborators",
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
            })
            .then(res => res.json())
    },

    getCollaboratorById: function (token, collaboratorId) {
        //inspect the value
    },

    deleteCollaborator: function (token, collaboratorId) {
        return fetch(`http://localhost:3000/api/v1/collaborators/${collaboratorId}`,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
            });
    },

    editCollaborator: function (token, collaboratorId, body) {
        return fetch(`http://localhost:3000/api/v1/collaborators/${collaboratorId}`,
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
                body: JSON.stringify(body)
            })
            .then(res => res.json());
    },

    createCollaborator: function (token, collaborator) {
        return fetch(`http://localhost:3000/api/v1/collaborators`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
                body: JSON.stringify(collaborator)
            })
            .then(res => res.json());
    }
};

export default CollaboratorService;
