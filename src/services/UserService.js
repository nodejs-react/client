const UserService = {
    getUsers: function (token) {
        return fetch("http://localhost:3000/api/v1/users",
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
            })
            .then(res => res.json())
    },

    deleteUser: function (token, userId) {
        return fetch(`http://localhost:3000/api/v1/users/${userId}`,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
            })
    },

    editUser: function (token, user, body) {
        return fetch(`http://localhost:3000/api/v1/users/${user._id}`,
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
                body: JSON.stringify(body)
            })
            .then(res => res.json());
    },

    createUser: function (token, user) {
        return fetch(`http://localhost:3000/api/v1/users`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                },
                body: JSON.stringify(user)
            })
            .then(res => res.json())
    }
};

export default UserService;
