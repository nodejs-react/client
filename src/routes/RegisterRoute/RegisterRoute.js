import {
    Route,
} from "react-router-dom";
import React from "react";
import Register from "../../components/Register/Register";

function RegisterRoute() {
    return (
        <Route
            render={(props) => {
                return <Register/>;
            }
            }
        />
    );
}

export default RegisterRoute;
