import {
    Redirect,
    Route,
} from "react-router-dom";
import React from "react";
import {useAuth} from "../../context/auth";
import Login from "../../components/Login/Login";

function LoginRoute() {
    const isAuthenticated = useAuth();

    return (
        <Route
            render={props =>
                isAuthenticated.authTokens ? (
                    <Redirect to="/"/>
                ) : <Login/>
            }
        />
    );
}

export default LoginRoute;
