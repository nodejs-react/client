import {
    Redirect,
    Route,
} from "react-router-dom";
import React from "react";
import {useAuth} from "../../context/auth";

function AuthenticatedRoute({component: Component, ...rest}) {
    const isAuthenticated = useAuth();

    return (
        <Route
            {...rest}
            render={props =>
                isAuthenticated.authTokens ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{pathname: "/login", state: {referer: props.location}}}
                    />
                )
            }
        />
    );
}

export default AuthenticatedRoute;
