import React, {useState} from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
} from "react-router-dom";
import {AuthContext} from "./context/auth";
import AuthenticatedRoute from "./routes/AuthenticatedRoute/AuthenticatedRoute";
import LoginRoute from "./routes/LoginRoute/LoginRoute";
import Home from "./components/Home/Home";
import RegisterRoute from "./routes/RegisterRoute/RegisterRoute";

function App() {
    const [authTokens, setAuthTokens] = useState(JSON.parse(localStorage.getItem("js-app-tokens")));
    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem("js-app-user")));

    const setTokens = (data) => {
        localStorage.setItem("js-app-tokens", JSON.stringify(data));
        setAuthTokens(data);
    };

    const setUser = (user) => {
        localStorage.setItem("js-app-user", JSON.stringify(user));
        setCurrentUser(user);
    };

    return (
        <AuthContext.Provider value={{authTokens, setAuthTokens: setTokens, currentUser, setCurrentUser: setUser}}>
            <Router>
                <Switch>
                    <RegisterRoute
                        path="/register"
                    />
                    <LoginRoute
                        path="/login"
                    />
                    <AuthenticatedRoute
                        path="/home"
                        component={Home}
                    />
                    <AuthenticatedRoute
                        path="/"
                        component={Home}
                    />
                </Switch>
            </Router>
        </AuthContext.Provider>
    );
}

export default App;
