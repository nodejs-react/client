import * as React from "react";

export class NotFound extends React.Component {
    render() {
        return <h2>Page not found!</h2>;
    }
}
