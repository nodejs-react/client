import {Form, Input, Button} from 'antd';
import {UserOutlined, MailOutlined, KeyOutlined, PhoneOutlined, ArrowLeftOutlined} from '@ant-design/icons';
import React from "react";
import './Register.css';
import {useHistory} from "react-router";

export default function Register(props) {
    const history = useHistory();

    const onFinish = (values) => {
        fetch("http://localhost:3000/api/v1/auth/register",
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(values)
            })
            .then(res => res.json())
            .then(
                () => {
                    history.push('/login');
                },
                (error) => {
                    console.log(error);
                }
            )
    };

    return (
        <div className="register-container">
            <Button className="back-button" type="dashed" shape="circle" icon={<ArrowLeftOutlined/>}
                    onClick={() => {
                        history.push('/login');
                    }}/>
            <Form
                name="normal_login"
                className="login-form"
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    rules={[{required: true, message: 'Merci de renseigner un e-mail!'}]}
                >
                    <Input prefix={<MailOutlined className="site-form-item-icon"/>}
                           autoComplete="nope"
                           placeholder="Email"/>
                </Form.Item>
                <Form.Item
                    name="password"
                    autoComplete="new-password"
                    rules={[{required: true, message: 'Merci de renseigner un mot de passe!'}]}
                >
                    <Input prefix={<KeyOutlined className="site-form-item-icon"/>}
                           autoComplete={'new-password'}
                           type="password"
                           placeholder="Mot de passe"/>
                </Form.Item>
                <Form.Item
                    name="firstName"
                    className="firstName-input"
                    rules={[{required: true, message: 'Merci de renseigner un prénom!'}]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        autoComplete="nope"
                        placeholder="Prénom"
                    />
                </Form.Item>
                <Form.Item
                    name="lastName"
                    className="lastName-input"
                    rules={[{required: true, message: 'Merci de renseigner un nom!'}]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        autoComplete="nope"
                        placeholder="Nom"
                    />
                </Form.Item>
                <Form.Item
                    name="phone"
                    className="phone-input"
                    rules={[{required: true, message: 'Merci de renseigner un numéro de téléphone!'}]}
                >
                    <Input
                        prefix={<PhoneOutlined className="site-form-item-icon"/>}
                        autoComplete="nope"
                        type="phone"
                        placeholder="Numéro de téléphone"
                    />
                </Form.Item>
                <Form.Item className="register-button">
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        S'INSCRIRE
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}
