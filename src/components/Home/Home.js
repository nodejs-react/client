import React, {useState} from 'react';
import MapPerso from '../Pages/CollaboratorsMap/MapPerso/MapPerso';
import {Layout, Menu} from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    LogoutOutlined,
    UserOutlined,
    PushpinOutlined,
} from '@ant-design/icons';
import './Home.css';
import {NotFound} from "../utils/NotFound/NotFound";
import Manager from "../Pages/ManagerPanel/Manager/Manager";
import Admin from "../Pages/AdminPanel/Admin/Admin";
import ReconciliationOutlined from "@ant-design/icons/lib/icons/ReconciliationOutlined";
import {useAuth} from "../../context/auth";
import {useHistory} from "react-router-dom";

const {Header, Sider} = Layout;

export default function Home(props) {
    const history = useHistory();
    const {setAuthTokens} = useAuth();
    const {currentUser} = useAuth();
    const [collapse, setCollapse] = useState(false);
    const [currentPage, setCurrentPage] = useState('MAP');

    function toggle() {
        setCollapse(!collapse);
    }

    function chosePage(page) {
        setCurrentPage(page);
    }

    function getComponent() {
        switch (currentPage) {
            case 'MAP':
                return <MapPerso/>;
            case 'ADMIN':
                return <Admin/>;
            case 'MANAGER':
                return <Manager/>;
            default:
                return <NotFound/>;
        }
    }

    function logout() {
        setAuthTokens(null);
        history.push('/login');
    }

    return (
        <Layout>
            <Sider trigger={null} collapsible collapsed={collapse}>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['MAP']}>
                    <Menu.Item key="MAP" icon={<PushpinOutlined/>} onClick={() => chosePage('MAP')}>
                        Carte
                    </Menu.Item>
                    {
                        currentUser && (currentUser.role === 'ADMIN' || currentUser.role === 'MANAGER') &&
                        <Menu.Item key="MANAGER" onClick={() => chosePage('MANAGER')} icon={<UserOutlined/>}>
                            Espace gestionnaire
                        </Menu.Item>
                    }
                    {
                        currentUser && currentUser.role === 'ADMIN' &&
                        <Menu.Item key="ADMIN" onClick={() => chosePage('ADMIN')} icon={<ReconciliationOutlined/>}>
                            Espace admin
                        </Menu.Item>
                    }
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{padding: 0}}>
                    {React.createElement(collapse ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: toggle,
                    })}
                    <div className={'user-name-block'}>
                            <UserOutlined className={'user-logo'}/>
                            <span className={'user-name'}>{`${currentUser.firstName} ${currentUser.lastName}`}</span>
                    </div>
                    {React.createElement(LogoutOutlined, {
                        className: 'logout',
                        onClick: logout,
                    })}
                </Header>
                {getComponent()}
            </Layout>
        </Layout>
    );
}
