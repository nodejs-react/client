import React, {useEffect, useState} from 'react';
import './Manager.css';
import {useAuth} from "../../../../context/auth";
import Table from "antd/es/table";
import {Button, Modal, Space, Tabs} from 'antd';
import collaboratorColumns from "../../collaborator_columns";
import {Layout} from "antd";
import {DeleteOutlined, EditOutlined, UserAddOutlined} from "@ant-design/icons";
import ManagerEditModal from "../ManagerEditModal/ManagerEditModal";
import ManagerCreateModal from "../ManagerCreateModal/ManagerCreateModal";
import CollaboratorService from "../../../../services/CollaboratorService";

const {Content} = Layout;
const {TabPane} = Tabs;


export default function Manager(props) {
    const [dataSourceManufacturers, setDataSourceManufacturers] = useState([]);
    const [dataSourceCustomers, setDataSourceCustomers] = useState([]);
    const [selectedCollaborator, setSelectedCollaborator] = useState(null);
    const [isDeleting, setIsDeleting] = useState(false);
    const [isUpdating, setIsUpdating] = useState(false);
    const [isCreating, setIsCreating] = useState(false);
    const {authTokens} = useAuth();
    let columns = collaboratorColumns();

    useEffect(() => {
        CollaboratorService.getCollaborators(authTokens).then(
            (result) => {
                if (result && result.length > 0) {
                    processData(result);
                }
            }
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function processData(result) {
        const manufacturersTable = [];
        const customersTable = [];
        result.forEach(value => {
            if (value.type === 'CUSTOMER') {
                customersTable.push(value);
            }
            if (value.type === 'MANUFACTURER') {
                manufacturersTable.push(value);
            }
        });
        setDataSourceCustomers(customersTable);
        setDataSourceManufacturers(manufacturersTable);
    }

    columns.push({
        title: 'Actions',
        render: (record) => (
            <Space size="middle">
                <EditOutlined onClick={() => {
                    setStates(false, true, false, record);
                }}/>
                <DeleteOutlined onClick={() => {
                    setStates(true, false, false, record);
                }}/>
            </Space>
        ),
    });

    function deleteEntity() {
        if (selectedCollaborator) {
            selectedCollaborator.deleteCollaborator(authTokens, selectedCollaborator._id).then(
                (response) => {
                    if (response && response.status === 200) {
                        if (selectedCollaborator.type === 'MANUFACTURER') {
                            let tab = [...dataSourceManufacturers];
                            const indexToRemove = tab.findIndex(v => v._id === selectedCollaborator._id);
                            tab.splice(indexToRemove, 1);
                            setDataSourceManufacturers(tab);
                        }
                        if (selectedCollaborator.type === 'CUSTOMER') {
                            let tab = [...dataSourceCustomers];
                            const indexToRemove = tab.findIndex(v => v._id === selectedCollaborator._id);
                            tab.splice(indexToRemove, 1);
                            setDataSourceCustomers(tab);
                        }
                        setStates(false, false, false, null);
                    }
                }
            );
        }
    }

    function onEdit(collab) {
        if (selectedCollaborator && selectedCollaborator._id) {
            CollaboratorService.editCollaborator(authTokens, selectedCollaborator._id, collab).then(
                (result) => {
                    let tabManufacturers = [...dataSourceManufacturers];
                    let index = tabManufacturers.findIndex(value => value._id === result._id);
                    if (index > -1) {
                        tabManufacturers[index] = result;
                        setDataSourceManufacturers(tabManufacturers);
                    } else {
                        let tabCustomers = [...dataSourceCustomers];
                        index = tabCustomers.findIndex(value => value._id === result._id);
                        if (index > -1) {
                            tabCustomers[index] = result;
                            setDataSourceCustomers(tabCustomers);
                        }
                    }
                    setStates(false, false, false, null);
                }
            );
        }
    }

    function onCreate(collaborator) {
        CollaboratorService.createCollaborator(authTokens, collaborator).then(
            (result) => {
                if (result.type === 'MANUFACTURER') {
                    let tab = [...dataSourceManufacturers];
                    tab.push(result);
                    setDataSourceManufacturers(tab);
                }
                if (result.type === 'CUSTOMER') {
                    let tab = [...dataSourceCustomers];
                    tab.push(result);
                    setDataSourceCustomers(tab);
                }
                setStates(false, false, false, null);
            }
        );
    }

    function setStates(isDeleting, isUpdating, isCreating, selectedCollaborator) {
        setIsDeleting(isDeleting);
        setIsUpdating(isUpdating);
        setIsCreating(isCreating);
        setSelectedCollaborator(selectedCollaborator);
    }

    return (
        <Content className="manager-container">
            <Button className={'add-user-button'}
                    type="default"
                    shape="round"
                    icon={<UserAddOutlined/>}
                    size={'large'}
                    onClick={() => {
                        setStates(false, false, true, null);
                    }}>
                Ajouter un collaborateur
            </Button>
            <Tabs defaultActiveKey="customers">
                <TabPane tab="Clients" key="customers">
                    <Table dataSource={dataSourceCustomers} columns={columns} rowKey="_id"/>
                </TabPane>
                <TabPane tab="Fabriquants" key="manufacturers">
                    <Table dataSource={dataSourceManufacturers} columns={columns} rowKey="_id"/>
                </TabPane>
            </Tabs>
            {(selectedCollaborator && isDeleting) ? <Modal
                title="Confirmation de suppression"
                centered
                visible={true}
                onOk={() => deleteEntity()}
                onCancel={() => {
                    setStates(false, false, false, null);
                }}
            >
                <p>Voulez-vous vraiment supprimer ce collaborateur?</p>
            </Modal> : null}
            {(selectedCollaborator && isUpdating) ? <ManagerEditModal
                editedCollaborator={selectedCollaborator}
                onEditEvent={(collab) => onEdit(collab)}
                onCancelEvent={() => {
                    setStates(false, false, false, null);
                }}
            /> : null}
            {(isCreating) ? <ManagerCreateModal
                onCreateEvent={(user) => onCreate(user)}
                onCancelEvent={() => {
                    setStates(false, false, false, null);
                }}
            /> : null}
        </Content>
    );
}
