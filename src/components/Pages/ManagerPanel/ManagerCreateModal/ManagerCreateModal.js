import React from 'react';
import {Form, Input, Modal, Select, InputNumber} from 'antd';
import {UserOutlined, BankOutlined} from "@ant-design/icons";

export default function ManagerCreateModal(props) {
    const [form] = Form.useForm();
    const {Option} = Select;
    return (
        <Modal
            title="Création d'un collaborateur"
            okText="Confirmer"
            cancelText="Annuler"
            visible={true}
            onCancel={props?.onCancelEvent}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        if (props && props.onCreateEvent) {
                            props.onCreateEvent(values);
                        }
                    })
                    .catch(info => {
                        console.log('Validation Failed:', info);
                    });
            }}>
            <Form
                form={form}
                name="normal_login"
                className="login-form"
            >
                <Form.Item
                    name="companyName"
                    rules={[{required: true, message: "Merci de renseigner le nom de l'entreprise"}]}>
                    <Input prefix={<BankOutlined className="site-form-item-icon"/>}
                           placeholder="Nom de l'entreprise"/>
                </Form.Item>
                <Form.Item
                    name="siret"
                    rules={[{required: true, message: 'Merci de renseigner le SIRET!'},
                        {pattern: /^[0-9]{10}$/, message: "Un SIRET contient 10 chiffres."}]}>
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        placeholder="SIRET"
                    />
                </Form.Item>
                <Form.Item
                    name="type"
                    rules={[{required: true, message: 'Merci de renseigner un nom!'},
                        {type: "enum", enum: ['CUSTOMER', 'MANUFACTURER']}]}>
                    <Select placeholder="Type" style={{width: 150}}>
                        <Option value="CUSTOMER">CUSTOMER</Option>
                        <Option value="MANUFACTURER">MANUFACTURER</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="latitude"
                    label="Latitude"
                    rules={[{required: true, message: 'Merci de renseigner une latitude.'}]}
                >
                    <InputNumber
                        placeholder="Latitude"
                        precision={4}
                        style={{width: 150}}
                    />
                </Form.Item>
                <Form.Item
                    name="longitude"
                    label="Longitude"
                    rules={[{required: true, message: 'Merci de renseigner une longitude.'}]}
                >
                    <InputNumber
                        placeholder="Longitude"
                        precision={4}
                        style={{width: 150}}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
}
