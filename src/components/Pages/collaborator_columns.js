export default function collaboratorColumns() {
    return [
        {
            title: 'SIRET',
            dataIndex: 'siret',
        },
        {
            title: 'Entreprise',
            dataIndex: 'companyName',
        },
        {
            title: 'Latitude',
            dataIndex: 'latitude',
        },
        {
            title: 'Longitude',
            dataIndex: 'longitude',
        },
    ];
}
