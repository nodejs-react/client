import React, {useEffect, useState} from 'react';
import './Admin.css';
import Table from "antd/es/table";
import userColumns from '../../user_columns';
import {useAuth} from "../../../../context/auth";
import {Button, Layout, Modal, Space} from "antd";
import {DeleteOutlined, EditOutlined, UserAddOutlined} from "@ant-design/icons";
import UserEditModal from "../UserEditModal/UserEditModal";
import UserCreateModal from "../UserCreateModal/UserCreateModal";
import UserService from '../../../../services/UserService';

const {Content} = Layout;


export default function Admin(props) {
    const [dataSource, setDataSource] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [isDeleting, setIsDeleting] = useState(false);
    const [isUpdating, setIsUpdating] = useState(false);
    const [isCreating, setIsCreating] = useState(false);
    const {authTokens} = useAuth();
    let columns = userColumns();

    useEffect(() => {
        UserService.getUsers(authTokens).then(result => {
            if (result && result.length > 0) {
                setDataSource(result);
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedUser]);

    columns.push({
        title: 'Actions',
        render: (record) => (
            <Space size="middle">
                <EditOutlined onClick={() => {
                    setStates(false, true, false, record);
                }}/>
                <DeleteOutlined onClick={() => {
                    setStates(true, false, false, record);
                }}/>
            </Space>
        ),
    });

    function deleteUser() {
        if (selectedUser) {
            UserService.deleteUser(authTokens, selectedUser._id).then(
                (response) => {
                    if (response && response.status === 200) {
                        setStates(false, false, false, null);
                        const tab = [...dataSource];
                        const indexToRemove = tab.findIndex(v => v._id === selectedUser._id);
                        tab.splice(indexToRemove, 1);
                        setDataSource(tab);
                    }
                }
            )
        }
    }

    function onEdit(user) {
        if (selectedUser && selectedUser._id) {
            UserService.editUser(authTokens, selectedUser, user).then(
                (result) => {
                    let tab = [...dataSource];
                    const index = tab.findIndex(value => value._id === result._id);
                    if (index > -1) {
                        tab[index] = result;
                    }
                    setStates(false, false, false, null);
                }
            );
        }
    }

    function onCreate(user) {
        UserService.createUser(user).then(
            (result) => {
                let tab = [...dataSource];
                tab.push(result);
                setDataSource(tab);
                setStates(false, false, false, null);
            }
        );
    }

    function setStates(isDeleting, isUpdating, isCreating, selectedUser) {
        setIsDeleting(isDeleting);
        setIsUpdating(isUpdating);
        setIsCreating(isCreating);
        setSelectedUser(selectedUser);
    }

    return (
        <Content className="admin-container">
            <Button className={'add-user-button'}
                    type="default"
                    shape="round"
                    icon={<UserAddOutlined/>}
                    size={'large'}
                    onClick={() => {
                        setStates(false, false, true, null);
                    }}>
                Ajouter un utilisateur
            </Button>
            <Table dataSource={dataSource} rowKey="_id" columns={columns}/>
            {(isDeleting && selectedUser) ? <Modal
                title="Confirmation de suppression"
                centered
                visible={true}
                onOk={() => deleteUser()}
                onCancel={function () {
                    setStates(false, false, false, null);
                }}
            >
                <p>Voulez-vous vraiment supprimer cet utilisateur?</p>
            </Modal> : null}
            {(selectedUser && isUpdating) ? <UserEditModal
                editedUser={selectedUser}
                onEditEvent={(user) => onEdit(user)}
                onCancelEvent={() => {
                    setStates(false, false, false, null);
                }}
            /> : null}
            {(isCreating) ? <UserCreateModal
                onCreateEvent={(user) => onCreate(user)}
                onCancelEvent={() => {
                    setStates(false, false, false, null);
                }}
            /> : null}
        </Content>
    );
}
