import React from 'react';
import './UserCreateModal.css';
import {Form, Input, Modal} from 'antd';
import {UserOutlined, MailOutlined, PhoneOutlined, KeyOutlined} from "@ant-design/icons";
import Select from "antd/es/select";


export default function UserCreateModal(props) {
    const [form] = Form.useForm();

    return (
        <Modal
            title="Création d'un utilisateur"
            okText="Créer"
            cancelText="Annuler"
            visible={true}
            onCancel={props?.onCancelEvent}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        if (props && props.onCreateEvent) {
                            props.onCreateEvent(values);
                        }
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                name="normal_login"
                className="login-form"
            >
                <Form.Item
                    name="email"
                    rules={[{required: true, message: 'Merci de renseigner un e-mail!'}]}
                >
                    <Input prefix={<MailOutlined className="site-form-item-icon"/>}
                           autoComplete="nope"
                           placeholder="Email"/>
                </Form.Item>
                <Form.Item
                    name="password"
                    autoComplete="new-password"
                    rules={[{required: true, message: 'Merci de renseigner un mot de passe!'}]}
                >
                    <Input prefix={<KeyOutlined className="site-form-item-icon"/>}
                           type="password"
                           placeholder="Mot de passe"/>
                </Form.Item>
                <Form.Item
                    name="firstName"
                    className="firstName-input"
                    rules={[{required: true, message: 'Merci de renseigner un prénom!'}]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        autoComplete="nope"
                        placeholder="Prénom"
                    />
                </Form.Item>
                <Form.Item
                    name="lastName"
                    className="lastName-input"
                    rules={[{required: true, message: 'Merci de renseigner un nom!'}]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        autoComplete="nope"
                        placeholder="Nom"
                    />
                </Form.Item>
                <Form.Item
                    name="phone"
                    className="phone-input"
                    rules={[{required: true, message: 'Merci de renseigner un numéro de téléphone!'}]}
                >
                    <Input
                        prefix={<PhoneOutlined className="site-form-item-icon"/>}
                        autoComplete="nope"
                        type="phone"
                        placeholder="Numéro de téléphone"
                    />
                </Form.Item>
                <Form.Item name="role"
                           className="role-input"
                           rules={[{required: true, message: 'Merci de renseigner un role!'}]}>
                    <Select
                        placeholder="Rôle"
                    >
                        <Select.Option value="ADMIN">Administrateur</Select.Option>
                        <Select.Option value="MANAGER">Gestionnaire</Select.Option>
                        <Select.Option value="SIMPLE">Simple</Select.Option>
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
}
