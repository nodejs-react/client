import React from 'react';
import './UserEditModal.css';
import {Form, Input, Modal} from 'antd';
import {UserOutlined, MailOutlined, PhoneOutlined} from "@ant-design/icons";
import Select from "antd/es/select";


export default function UserEditModal(props) {
    const [form] = Form.useForm();

    return (
        <Modal
            title="Édition d'un utilisateur"
            okText="Confirmer"
            cancelText="Annuler"
            visible={true}
            onCancel={props?.onCancelEvent}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        if (props && props.onEditEvent) {
                            props.onEditEvent(values);
                        }
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                name="normal_login"
                className="login-form"
                initialValues={{
                    email: props?.editedUser?.email,
                    firstName: props?.editedUser?.firstName,
                    lastName: props?.editedUser?.lastName,
                    phone: props?.editedUser?.phone,
                    role: props?.editedUser?.role
                }}
            >
                <Form.Item
                    name="email"
                    rules={[{required: true, message: 'Merci de renseigner votre e-mail!'}]}
                >
                    <Input prefix={<MailOutlined className="site-form-item-icon"/>}
                           placeholder="Email"/>
                </Form.Item>
                <Form.Item
                    name="firstName"
                    className="firstName-input"
                    rules={[{required: true, message: 'Merci de renseigner un prénom!'}]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        placeholder="Prénom"
                    />
                </Form.Item>
                <Form.Item
                    name="lastName"
                    className="lastName-input"
                    rules={[{required: true, message: 'Merci de renseigner un nom!'}]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        placeholder="Nom"
                    />
                </Form.Item>
                <Form.Item
                    name="phone"
                    className="phone-input"
                    rules={[{required: true, message: 'Merci de renseigner un numéro de téléphone!'}]}
                >
                    <Input
                        prefix={<PhoneOutlined className="site-form-item-icon"/>}
                        type="phone"
                        placeholder="Numéro de téléphone"
                    />
                </Form.Item>
                <Form.Item name="role"
                           className="role-input"
                           rules={[{required: true, message: 'Merci de renseigner un role!'}]}>
                    <Select
                        placeholder="Rôle"
                    >
                        <Select.Option value="ADMIN">Administrateur</Select.Option>
                        <Select.Option value="MANAGER">Gestionnaire</Select.Option>
                        <Select.Option value="SIMPLE">Simple</Select.Option>
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
}
