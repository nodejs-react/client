import React, {useState, useEffect} from 'react'
import {Map, TileLayer, Marker, Popup} from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import './MapPerso.css'
import {useAuth} from "../../../../context/auth";
import Pin from '../Pin/Pin';
import L from 'leaflet';
import ManagerCreateForm from "../ManagerCreateForm/ManagerCreateForm";
import CollaboratorService from "../../../../services/CollaboratorService";

function MapPerso() {
    const {authTokens} = useAuth();
    const state = {
        lat: 51.505,
        lng: -0.09,
        zoom: 13,
    };
    const [pins, setPins] = useState([]);
    const [currentPosition, setCurrentPosition] = useState();
    const [isAuxMenu, setIsAuxMenu] = useState(false);

    useEffect(() => {
        currentLocation();
        CollaboratorService.getCollaborators(authTokens).then(
            (result) => {
                let newPins = [];
                result.forEach(element => {
                    newPins.push(
                        Pin(element.longitude, element.latitude,
                            {
                                firstName: element.firstName ? element.firstName : null,
                                lastName: element.lastName ? element.lastName : null,
                                type: element.type,
                                companyName: element.companyName ? element.companyName : null,
                                siret: element.siret ? element.siret : null
                            })
                    );
                });
                setPins(newPins);
            }
        );
        // eslint-disable-next-line
    }, []);

    function currentLocation() {
        setCurrentPosition([state.lat, state.lng]);
        navigator.geolocation.getCurrentPosition(
            (geoLoc) => {
                setCurrentPosition([geoLoc.coords.latitude, geoLoc.coords.longitude]);
            },
            () => {
                setCurrentPosition([state.lat, state.lng]);
            },
            {maximumAge: 0});
    }

    function iconAndroid(color) {
        let androidURL = require('../../../../assets/image/icon-android-noir.svg');
        if (color === "green") androidURL = require('../../../../assets/image/icon-android-vert.svg');
        if (color === "blue") androidURL = require('../../../../assets/image/icon-android-bleu.svg');
        return new L.Icon({
            iconUrl: androidURL,
            iconRetinaUrl: androidURL,
            popupAnchor: [10, -20],
            iconSize: new L.Point(50, 61),
            className: 'leaflet-marker-icon'
        });
    }

    function locate(ev) {
        setCurrentPosition(ev.latlng);
        setIsAuxMenu(true);
    }

    function validateNewCollaborator(collaborator) {
        CollaboratorService.createCollaborator(authTokens, collaborator).then(
            (result) => {
                const newPins = [...pins];
                newPins.push(
                    Pin(result.longitude, result.latitude,
                        {
                            firstName: result.firstName ? result.firstName : null,
                            lastName: result.lastName ? result.lastName : null,
                            type: result.type,
                            companyName: result.companyName ? result.companyName : null,
                            siret: result.siret ? result.siret : null
                        })
                );
                setPins(newPins);
                setIsAuxMenu(false);
            }
        );
    }

    return (
        <Map center={currentPosition} zoom={state.zoom} onclick={(ev) => locate(ev)}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {pins.map((p, index) => {
                return <Marker position={[p.lat, p.long]} key={index}
                               icon={p.descr.type === "CUSTOMER" ? iconAndroid("green") : iconAndroid("black")}>
                    <Popup className="markerPopup">
                        {p.descr.firstName ? p.descr.firstName + " " + p.descr.lastName : p.descr.companyName} <br/>
                        <p style={{color: 'lightcoral'}}>{p.descr.type}</p>
                        {p.descr.siret ? "SIRET: " + p.descr.siret : null}
                    </Popup>
                </Marker>
            })}
            {(isAuxMenu) ? <Marker position={currentPosition} icon={iconAndroid("blue")} key={101} draggable="true">
                <Popup className="create-collaborator-popup" onClose={(ev) => setIsAuxMenu(false)}>
                    <ManagerCreateForm position={currentPosition}
                                       validationFunc={validateNewCollaborator}>
                    </ManagerCreateForm>
                </Popup>
            </Marker> : null}
        </Map>
    );
}

export default MapPerso;
