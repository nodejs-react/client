import React from 'react'
import {Form, Input, Radio, InputNumber, Button} from 'antd';
import {UserOutlined, BankOutlined} from "@ant-design/icons";
import './ManagerCreateForm.css';


export default function ManagerCreateForm(props) {
    const [form] = Form.useForm();

    const onFinish = newCollaborator => {
        props.validationFunc(newCollaborator);
    };

    return (
        <Form
            form={form}
            id="create-form-id"
            name="create-collaborator-form"
            className="create-collaborator-form"
            layout="vertical"
            initialValues={{
                type: 'CUSTOMER',
                longitude: props?.position.lng,
                latitude: props?.position.lat
            }}
            onFinish={onFinish}>
            <Form.Item
                style={{maxHeight: 0}}>
            </Form.Item>
            <Form.Item
                name="companyName"
                rules={[{required: true, message: "Merci de renseigner votre nom d'Entreprise"}]}>
                <Input prefix={<BankOutlined className="site-form-item-icon"/>}
                       placeholder="Nom d'Entreprise"/>
            </Form.Item>
            <Form.Item
                name="siret"
                rules={[{required: true, message: 'Merci de renseigner le SIRET!'},
                    {pattern: /^[0-9]{10}$/, message: "10 chiffres s'il vous plait!"}]}>
                <Input
                    prefix={<UserOutlined className="site-form-item-icon"/>}
                    placeholder="SIRET"
                />
            </Form.Item>
            <Form.Item
                name="type"
                rules={[{required: true, message: 'Le type est O-bli-ga-toire!'},
                    {type: "enum", enum: ['CUSTOMER', 'MANUFACTURER']}]}>
                <Radio.Group defaultValue="CUSTOMER" buttonStyle="solid">
                    <Radio.Button value="CUSTOMER">CLIENT</Radio.Button>
                    <Radio.Button value="MANUFACTURER">FOURNISSEUR</Radio.Button>
                </Radio.Group>
            </Form.Item>
            <Form.Item
                name="longitude"
                label="Longitude"
                rules={[{required: true, message: 'Merci de renseigner votre longitude'}]}
                className={'lat-lon-item'}
            >
                <InputNumber
                    className={'lat-lon-input'}
                    precision={14}
                    style={{width: 150}}
                    placeholder="Longitude"
                />
            </Form.Item>
            <Form.Item
                name="latitude"
                label="Latitude"
                rules={[{required: true, message: 'Merci de renseigner votre Latitude'}]}
                className={'lat-lon-item'}
            >
                <InputNumber
                    className={'lat-lon-input'}
                    precision={14}
                    style={{width: 150}}
                    placeholder="Latitude"
                />
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType="submit">
                    Ajouter
                </Button>
            </Form.Item>
        </Form>
    )
}
