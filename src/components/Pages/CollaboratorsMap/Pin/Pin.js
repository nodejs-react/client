function Pin(long, lat, description) {
	let pin = {
		long: long,
		lat: lat,
		descr: description
	}
	return pin;
}

export default Pin;