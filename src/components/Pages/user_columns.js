export default function userColumns() {
    return [
        {
            title: 'Prénom',
            dataIndex: 'firstName',
        },
        {
            title: 'Nom',
            dataIndex: 'lastName',
        },
        {
            title: 'E-mail',
            dataIndex: 'email',
        },
        {
            title: 'Téléphone',
            dataIndex: 'phone',
        },
        {
            title: 'Rôle',
            dataIndex: 'role',
        },
    ];
}
