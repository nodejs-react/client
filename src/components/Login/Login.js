import {Form, Input, Button} from 'antd';
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import React, {useState} from "react";
import './Login.css';
import {useAuth} from "../../context/auth";
import {Redirect, useHistory} from "react-router";

export default function Login(props) {
    const [isLoggedIn, setLoggedIn] = useState(false);
    const {setAuthTokens} = useAuth();
    const {setCurrentUser} = useAuth();
    const history = useHistory();
    const referer = props.location?.state?.referer || '/';

    if (isLoggedIn) {
        return <Redirect to={referer}/>;
    }

    const onFinish = (values) => {
        fetch("http://localhost:3000/api/v1/auth/login",
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(values)
            })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result && result.token && result.user) {
                        setAuthTokens(result.token);
                        setCurrentUser(result.user);
                        setLoggedIn(true);
                    } else {
                        console.error('ERROR');
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    };

    return (
        <div className="login-container">
            <Form
                name="normal_login"
                className="login-form"
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    rules={[{required: true, message: 'Merci de renseigner votre e-mail!'}]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon"/>}
                           autoComplete="none"
                           placeholder="Email"/>
                </Form.Item>
                <Form.Item
                    name="password"
                    className="password-input"
                    rules={[{required: true, message: 'Merci de renseigner votre mot de passe!'}]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        autoComplete={'new-password'}
                        type="password"
                        placeholder="Mot de passe"
                    />
                </Form.Item>
                <Form.Item className="login-button">
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        CONNEXION
                    </Button>
                </Form.Item>
                <Form.Item>
                    <Button type="dashed" className="login-form-button" onClick={() => {
                        history.push('/register');
                    }}>
                        S'INSCRIRE
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}
